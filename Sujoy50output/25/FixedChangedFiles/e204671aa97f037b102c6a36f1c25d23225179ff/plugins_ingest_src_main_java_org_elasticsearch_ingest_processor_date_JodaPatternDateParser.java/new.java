/*
 * Licensed to Elasticsearch under one or more contributor
 * license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright
 * ownership. Elasticsearch licenses this file to you under
 * the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.elasticsearch.ingest.processor.date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

public class JodaPatternDateParser implements DateParser {

    private final DateTimeFormatter formatter;

    public JodaPatternDateParser(String format) {
        formatter = DateTimeFormat.forPattern(format)
                .withDefaultYear((new DateTime(DateTimeZone.UTC)).getYear())
                .withOffsetParsed();
    }

    public JodaPatternDateParser(String format, DateTimeZone timezone) {
        formatter = DateTimeFormat.forPattern(format)
                .withDefaultYear((new DateTime(timezone)).getYear())
                .withZone(timezone);
    }

    public JodaPatternDateParser(String format, Locale locale) {
        formatter = DateTimeFormat.forPattern(format)
                .withDefaultYear((new DateTime(DateTimeZone.UTC)).getYear())
                .withLocale(locale);
    }

    public JodaPatternDateParser(String format, DateTimeZone timezone, Locale locale) {
        formatter = DateTimeFormat.forPattern(format)
                .withDefaultYear((new DateTime(DateTimeZone.UTC)).getYear())
                .withZone(timezone).withLocale(locale);
    }

    @Override
    public long parseMillis(String date) {
        return formatter.parseMillis(date);
    }

    @Override
    public DateTime parseDateTime(String date) {
        return formatter.parseDateTime(date);
    }
}
