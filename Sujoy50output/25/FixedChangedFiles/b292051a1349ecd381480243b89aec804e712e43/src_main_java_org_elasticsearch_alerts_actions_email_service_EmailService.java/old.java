/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
package org.elasticsearch.alerts.actions.email.service;

import org.elasticsearch.cluster.ClusterState;

/**
 *
 */
public interface EmailService {

    void start(ClusterState state);

    void stop();

    EmailSent send(Email email, Authentication auth, Profile profile);

    EmailSent send(Email email, Authentication auth, Profile profile, String accountName);

    static class EmailSent {

        private final String account;
        private final Email email;

        public EmailSent(String account, Email email) {
            this.account = account;
            this.email = email;
        }

        public String account() {
            return account;
        }

        public Email email() {
            return email;
        }
    }

}
