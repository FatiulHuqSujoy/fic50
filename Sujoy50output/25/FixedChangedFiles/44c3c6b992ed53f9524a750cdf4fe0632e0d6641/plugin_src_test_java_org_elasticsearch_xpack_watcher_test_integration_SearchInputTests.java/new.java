/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
package org.elasticsearch.xpack.watcher.test.integration;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.search.ShardSearchFailure;
import org.elasticsearch.action.support.PlainActionFuture;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.NamedXContentRegistry;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentParser;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.script.MockMustacheScriptEngine;
import org.elasticsearch.script.ScriptContext;
import org.elasticsearch.script.ScriptEngine;
import org.elasticsearch.script.ScriptService;
import org.elasticsearch.search.SearchModule;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.internal.InternalSearchResponse;
import org.elasticsearch.test.ESTestCase;
import org.elasticsearch.xpack.watcher.Watcher;
import org.elasticsearch.xpack.watcher.condition.AlwaysCondition;
import org.elasticsearch.xpack.watcher.execution.TriggeredExecutionContext;
import org.elasticsearch.xpack.watcher.execution.WatchExecutionContext;
import org.elasticsearch.xpack.watcher.input.Input;
import org.elasticsearch.xpack.watcher.input.search.ExecutableSearchInput;
import org.elasticsearch.xpack.watcher.input.search.SearchInput;
import org.elasticsearch.xpack.watcher.input.search.SearchInputFactory;
import org.elasticsearch.xpack.watcher.input.simple.ExecutableSimpleInput;
import org.elasticsearch.xpack.watcher.input.simple.SimpleInput;
import org.elasticsearch.xpack.watcher.support.search.WatcherSearchTemplateRequest;
import org.elasticsearch.xpack.watcher.support.search.WatcherSearchTemplateService;
import org.elasticsearch.xpack.watcher.test.WatcherTestUtils;
import org.elasticsearch.xpack.watcher.trigger.schedule.IntervalSchedule;
import org.elasticsearch.xpack.watcher.trigger.schedule.ScheduleTrigger;
import org.elasticsearch.xpack.watcher.trigger.schedule.ScheduleTriggerEvent;
import org.elasticsearch.xpack.watcher.watch.Payload;
import org.elasticsearch.xpack.watcher.watch.Watch;
import org.elasticsearch.xpack.watcher.watch.WatchStatus;
import org.joda.time.DateTime;
import org.junit.Before;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static org.elasticsearch.common.unit.TimeValue.timeValueSeconds;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.mock.orig.Mockito.when;
import static org.elasticsearch.search.builder.SearchSourceBuilder.searchSource;
import static org.elasticsearch.xpack.watcher.test.WatcherTestUtils.getRandomSupportedSearchType;
import static org.hamcrest.Matchers.arrayContainingInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.joda.time.DateTimeZone.UTC;
import static org.mockito.Mockito.mock;

public class SearchInputTests extends ESTestCase {

    private ScriptService scriptService;
    private Client client;

    @Before
    public void setup() {
        Map<String, ScriptEngine> engines = new HashMap<>();
        engines.put(MockMustacheScriptEngine.NAME, new MockMustacheScriptEngine());
        Map<String, ScriptContext<?>> contexts = new HashMap<>();
        contexts.put(Watcher.SCRIPT_TEMPLATE_CONTEXT.name, Watcher.SCRIPT_TEMPLATE_CONTEXT);
        contexts.put(Watcher.SCRIPT_SEARCH_CONTEXT.name, Watcher.SCRIPT_SEARCH_CONTEXT);
        contexts.put(Watcher.SCRIPT_EXECUTABLE_CONTEXT.name, Watcher.SCRIPT_EXECUTABLE_CONTEXT);
        scriptService = new ScriptService(Settings.EMPTY, engines, contexts);

        client = mock(Client.class);
    }

    public void testExecute() throws Exception {
        ArgumentCaptor<SearchRequest> requestCaptor = ArgumentCaptor.forClass(SearchRequest.class);
        PlainActionFuture<SearchResponse> searchFuture = PlainActionFuture.newFuture();
        SearchResponse searchResponse = new SearchResponse(InternalSearchResponse.empty(), "", 1, 1, 1234, ShardSearchFailure.EMPTY_ARRAY);
        searchFuture.onResponse(searchResponse);
        when(client.search(requestCaptor.capture())).thenReturn(searchFuture);

        SearchSourceBuilder searchSourceBuilder = searchSource().query(boolQuery().must(matchQuery("event_type", "a")));

        WatcherSearchTemplateRequest request = WatcherTestUtils.templateRequest(searchSourceBuilder);
        ExecutableSearchInput searchInput = new ExecutableSearchInput(new SearchInput(request, null, null, null), logger,
                client, watcherSearchTemplateService(), TimeValue.timeValueMinutes(1));
        WatchExecutionContext ctx = createExecutionContext();
        SearchInput.Result result = searchInput.execute(ctx, new Payload.Simple());

        assertThat(result.status(), is(Input.Result.Status.SUCCESS));
        SearchRequest searchRequest = requestCaptor.getValue();
        assertThat(searchRequest.searchType(), is(request.getSearchType()));
        assertThat(searchRequest.indicesOptions(), is(request.getIndicesOptions()));
        assertThat(searchRequest.indices(), is(arrayContainingInAnyOrder(request.getIndices())));

    }

    private TriggeredExecutionContext createExecutionContext() {
        return new TriggeredExecutionContext(
                new Watch("test-watch",
                        new ScheduleTrigger(new IntervalSchedule(new IntervalSchedule.Interval(1, IntervalSchedule.Interval.Unit.MINUTES))),
                        new ExecutableSimpleInput(new SimpleInput(new Payload.Simple()), logger),
                        AlwaysCondition.INSTANCE,
                        null,
                        null,
                        new ArrayList<>(),
                        null,
                        new WatchStatus(new DateTime(0, UTC), emptyMap())),
                new DateTime(0, UTC),
                new ScheduleTriggerEvent("test-watch", new DateTime(0, UTC), new DateTime(0, UTC)),
                timeValueSeconds(5));
    }

    public void testDifferentSearchType() throws Exception {
        ArgumentCaptor<SearchRequest> requestCaptor = ArgumentCaptor.forClass(SearchRequest.class);
        PlainActionFuture<SearchResponse> searchFuture = PlainActionFuture.newFuture();
        SearchResponse searchResponse = new SearchResponse(InternalSearchResponse.empty(), "", 1, 1, 1234, ShardSearchFailure.EMPTY_ARRAY);
        searchFuture.onResponse(searchResponse);
        when(client.search(requestCaptor.capture())).thenReturn(searchFuture);

        SearchSourceBuilder searchSourceBuilder = searchSource().query(boolQuery().must(matchQuery("event_type", "a")));
        SearchType searchType = getRandomSupportedSearchType();
        WatcherSearchTemplateRequest request = WatcherTestUtils.templateRequest(searchSourceBuilder, searchType);

        ExecutableSearchInput searchInput = new ExecutableSearchInput(new SearchInput(request, null, null, null), logger,
                client, watcherSearchTemplateService(), TimeValue.timeValueMinutes(1));
        WatchExecutionContext ctx = createExecutionContext();
        SearchInput.Result result = searchInput.execute(ctx, new Payload.Simple());

        assertThat(result.status(), is(Input.Result.Status.SUCCESS));
        SearchRequest searchRequest = requestCaptor.getValue();
        assertThat(searchRequest.searchType(), is(request.getSearchType()));
        assertThat(searchRequest.indicesOptions(), is(request.getIndicesOptions()));
        assertThat(searchRequest.indices(), is(arrayContainingInAnyOrder(request.getIndices())));
    }

    public void testParserValid() throws Exception {
        SearchSourceBuilder source = searchSource()
                        .query(boolQuery().must(matchQuery("event_type", "a")).must(rangeQuery("_timestamp")
                                .from("{{ctx.trigger.scheduled_time}}||-30s").to("{{ctx.trigger.triggered_time}}")));

        TimeValue timeout = randomBoolean() ? TimeValue.timeValueSeconds(randomInt(10)) : null;
        XContentBuilder builder = jsonBuilder().value(new SearchInput(WatcherTestUtils.templateRequest(source), null, timeout, null));
        XContentParser parser = createParser(builder);
        parser.nextToken();

        SearchInputFactory factory = new SearchInputFactory(Settings.EMPTY, client, xContentRegistry(), scriptService);

        SearchInput searchInput = factory.parseInput("_id", parser);
        assertEquals(SearchInput.TYPE, searchInput.type());
        assertThat(searchInput.getTimeout(), equalTo(timeout));
    }

    // source: https://discuss.elastic.co/t/need-help-for-energy-monitoring-system-alerts/89415/3
    public void testThatEmptyRequestBodyWorks() throws Exception {
        ArgumentCaptor<SearchRequest> requestCaptor = ArgumentCaptor.forClass(SearchRequest.class);
        PlainActionFuture<SearchResponse> searchFuture = PlainActionFuture.newFuture();
        SearchResponse searchResponse = new SearchResponse(InternalSearchResponse.empty(), "", 1, 1, 1234, ShardSearchFailure.EMPTY_ARRAY);
        searchFuture.onResponse(searchResponse);
        when(client.search(requestCaptor.capture())).thenReturn(searchFuture);

        try (XContentBuilder builder = jsonBuilder().startObject().startObject("request")
                .startArray("indices").value("foo").endArray().endObject().endObject();
             XContentParser parser = XContentFactory.xContent(XContentType.JSON).createParser(NamedXContentRegistry.EMPTY,
                     builder.bytes())) {

            parser.nextToken(); // advance past the first starting object

            SearchInputFactory factory = new SearchInputFactory(Settings.EMPTY, client, xContentRegistry(), scriptService);
            SearchInput input = factory.parseInput("my-watch", parser);
            assertThat(input.getRequest(), is(not(nullValue())));
            assertThat(input.getRequest().getSearchSource(), is(BytesArray.EMPTY));

            ExecutableSearchInput executableSearchInput = factory.createExecutable(input);
            WatchExecutionContext ctx = createExecutionContext();
            SearchInput.Result result = executableSearchInput.execute(ctx, Payload.Simple.EMPTY);
            assertThat(result.status(), is(Input.Result.Status.SUCCESS));
            // no body in the search request
            ToXContent.Params params = new ToXContent.MapParams(Collections.singletonMap("pretty", "false"));
            assertThat(requestCaptor.getValue().source().toString(params), is("{}"));
        }
    }

    private WatcherSearchTemplateService watcherSearchTemplateService() {
        SearchModule module = new SearchModule(Settings.EMPTY, false, Collections.emptyList());
        return new WatcherSearchTemplateService(Settings.EMPTY, scriptService, new NamedXContentRegistry(module.getNamedXContents()));
    }
}
