/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
package org.elasticsearch.alerts.support;

/**
 *
 */
public final class Variables {

    public static final String FIRE_TIME = "fire_time";
    public static final String SCHEDULED_FIRE_TIME = "scheduled_fire_time";

}
