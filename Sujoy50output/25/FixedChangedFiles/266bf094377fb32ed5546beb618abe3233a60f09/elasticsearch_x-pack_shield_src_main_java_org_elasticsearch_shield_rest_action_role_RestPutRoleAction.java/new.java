/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
package org.elasticsearch.shield.rest.action.role;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.BytesRestResponse;
import org.elasticsearch.rest.RestChannel;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.RestResponse;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.rest.action.support.RestBuilderListener;
import org.elasticsearch.shield.action.role.PutRoleResponse;
import org.elasticsearch.shield.client.SecurityClient;

/**
 * Rest endpoint to add a Role to the shield index
 */
public class RestPutRoleAction extends BaseRestHandler {

    @Inject
    public RestPutRoleAction(Settings settings, RestController controller, Client client) {
        super(settings, client);
        controller.registerHandler(RestRequest.Method.POST, "/_shield/role/{name}", this);
        controller.registerHandler(RestRequest.Method.PUT, "/_shield/role/{name}", this);
    }

    @Override
    protected void handleRequest(RestRequest request, final RestChannel channel, Client client) throws Exception {
        new SecurityClient(client).preparePutRole(request.param("name"), request.content()).execute(
                new RestBuilderListener<PutRoleResponse>(channel) {
                    @Override
                    public RestResponse buildResponse(PutRoleResponse putRoleResponse, XContentBuilder builder) throws Exception {
                        return new BytesRestResponse(RestStatus.OK,
                                builder.startObject()
                                        .field("role", putRoleResponse)
                                        .endObject());
                    }
                });
    }
}
