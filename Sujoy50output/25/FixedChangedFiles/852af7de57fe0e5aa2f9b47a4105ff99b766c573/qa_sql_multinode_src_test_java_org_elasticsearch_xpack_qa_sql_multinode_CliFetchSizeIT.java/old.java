/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
package org.elasticsearch.xpack.qa.sql.multinode;

import org.elasticsearch.test.junit.annotations.TestLogging;
import org.elasticsearch.xpack.qa.sql.cli.FetchSizeTestCase;

@TestLogging("org.elasticsearch.xpack.qa.sql.multinode:DEBUG")
public class CliFetchSizeIT extends FetchSizeTestCase {
}
