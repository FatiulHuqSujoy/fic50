/*
 * Copyright 2002-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.expression.common;

import java.util.LinkedList;
import java.util.List;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParseException;
import org.springframework.expression.ParserContext;

/**
 * An expression parser that understands templates. It can be subclassed
 * by expression parsers that do not offer first class support for templating.
 *
 * @author Keith Donald
 * @author Juergen Hoeller
 * @author Andy Clement
 * @since 3.0
 */
public abstract class TemplateAwareExpressionParser implements ExpressionParser {

	/**
	 * Default ParserContext instance for non-template expressions.
	 */
	private static final ParserContext NON_TEMPLATE_PARSER_CONTEXT = new ParserContext() {
		public String getExpressionPrefix() {
			return null;
		}
		public String getExpressionSuffix() {
			return null;
		}
		public boolean isTemplate() {
			return false;
		}
	};


	public Expression parseExpression(String expressionString) throws ParseException {
		return parseExpression(expressionString, NON_TEMPLATE_PARSER_CONTEXT);
	}

	public Expression parseExpression(String expressionString, ParserContext context) throws ParseException {
		if (context == null) {
			context = NON_TEMPLATE_PARSER_CONTEXT;
		}
		if (context.isTemplate()) {
			return parseTemplate(expressionString, context);
		}
		else {
			return doParseExpression(expressionString, context);
		}
	}

	private Expression parseTemplate(String expressionString, ParserContext context) throws ParseException {
		if (expressionString.length() == 0) {
			return new LiteralExpression("");
		}
		Expression[] expressions = parseExpressions(expressionString, context);
		if (expressions.length == 1) {
			return expressions[0];
		} else {
			return new CompositeStringExpression(expressionString, expressions);
		}
	}


	/**
	 * Helper that parses given expression string using the configured parser. The expression string can contain any
	 * number of expressions all contained in "${...}" markers. For instance: "foo${expr0}bar${expr1}". The static
	 * pieces of text will also be returned as Expressions that just return that static piece of text. As a result,
	 * evaluating all returned expressions and concatenating the results produces the complete evaluated string.
	 * Unwrapping is only done of the outermost delimiters found, so the string 'hello ${foo${abc}}' would break into
	 * the pieces 'hello ' and 'foo${abc}'. This means that expression languages that used ${..} as part of their
	 * functionality are supported without any problem
	 * @param expressionString the expression string
	 * @return the parsed expressions
	 * @throws ParseException when the expressions cannot be parsed
	 */
	private Expression[] parseExpressions(String expressionString, ParserContext context) throws ParseException {
		List<Expression> expressions = new LinkedList<Expression>();
		int startIdx = 0;
		String prefix = context.getExpressionPrefix();
		String suffix = context.getExpressionSuffix();
		while (startIdx < expressionString.length()) {
			int prefixIndex = findUnescapedOccurenceOf(prefix,expressionString,startIdx);
			if (prefixIndex >= startIdx) {
				// an inner expression was found - this is a composite
				if (prefixIndex > startIdx) {
					expressions.add(createLiteralExpression(context,expressionString.substring(startIdx, prefixIndex)));
				}
				int afterPrefixIndex = prefixIndex + prefix.length();
				int suffixIndex = skipToCorrectEndSuffix(prefix,suffix,expressionString,afterPrefixIndex);
				if (suffixIndex == -1) {
					throw new ParseException(expressionString, "No ending suffix '" + suffix +
							"' for expression starting at character " + prefixIndex + ": " +
							expressionString.substring(prefixIndex));
				}
				if (suffixIndex == afterPrefixIndex) {
					throw new ParseException(expressionString, "No expression defined within delimiter '" +
							prefix + suffix + "' at character " + prefixIndex);
				} else {
					String expr = expressionString.substring(prefixIndex + prefix.length(), suffixIndex);
					expressions.add(doParseExpression(unescape(context,expr), context));
					startIdx = suffixIndex + suffix.length();
				}
			} else {
				// no more ${expressions} found in string, add rest as static text
				expressions.add(createLiteralExpression(context,expressionString.substring(startIdx)));
				startIdx = expressionString.length();
			}
		}
		return expressions.toArray(new Expression[expressions.size()]);
	}
	
	private Expression createLiteralExpression(ParserContext context, String text) {
		return new LiteralExpression(unescape(context,text));
	}

	/**
	 * Replace any escaped versions of the template prefix/suffix.  This will reduce, for example,  the string 
	 * 'hello\${ world' to 'hello${ world' - the escaping was only in the original to ensure the ${ was not
	 * treated as a real prefix.
	 */
	private String unescape(ParserContext context, String text) {
		String prefix = context.getExpressionPrefix();
		String suffix = context.getExpressionSuffix();
		String escapedPrefixesFixedUp = text.replace("\\"+prefix, prefix);
		String escapedSuffixesFixedUp = escapedPrefixesFixedUp.replace("\\"+suffix, suffix);
		return escapedSuffixesFixedUp;		
	}

	/**
	 * Find the next unescaped occurence of a particular string within an expression string.  An escaped version of the string 
	 * is preceeded by a backslash.  
	 */
	private int findUnescapedOccurenceOf(String toSearchFor, String expressionString, int startIdx) {
		int nextPossibility = -1;
		boolean foundOne = false;
		while (!foundOne) {
			nextPossibility = expressionString.indexOf(toSearchFor, startIdx);
			if (nextPossibility==-1) {
				return -1;
			} else {
				if (nextPossibility>0) {
					// check for the escape character
					if (expressionString.charAt(nextPossibility-1)!='\\') {
						// it was escaped, do not treat this as a real prefix
						foundOne = true;
					} else {
						startIdx = nextPossibility+toSearchFor.length();
					}
				} else {
					foundOne = true;
				}
			}
		}
		return nextPossibility;
	}

	/**
	 * Copes with nesting, for example '${...${...}}' where the correct end for the first ${ is the final }.
	 * 
	 * @param prefix the prefix 
	 * @param suffix the suffix
	 * @param expressionString the expression string
	 * @param afterPrefixIndex the most recently found prefix location for which the matching end suffix is being sought
	 * @return the position of the correct matching nextSuffix or -1 if none can be found
	 */
	private int skipToCorrectEndSuffix(String prefix, String suffix, String expressionString, int afterPrefixIndex) {
		int nextSuffix = findUnescapedOccurenceOf(suffix,expressionString,afterPrefixIndex);
		int nextPrefix = findUnescapedOccurenceOf(prefix,expressionString,afterPrefixIndex);
		if (nextPrefix==-1 || nextPrefix>nextSuffix) {
			return nextSuffix;
		} else {
			int depth = 0;
			while (nextPrefix!=-1 && nextPrefix<nextSuffix) {
				depth++;
				nextPrefix =  findUnescapedOccurenceOf(prefix,expressionString,nextPrefix+prefix.length());
			}
			while (depth>0) {
				depth--;
				nextSuffix = findUnescapedOccurenceOf(suffix, expressionString, nextSuffix+suffix.length());
				if (nextSuffix==-1) {
					return -1; // error case, not enough matching suffixes for prefixes
				}
			}
			return nextSuffix;
		}
	}

	/**
	 * Actually parse the expression string and return an Expression object.
	 * @param expressionString the raw expression string to parse
	 * @param context a context for influencing this expression parsing routine (optional)
	 * @return an evaluator for the parsed expression
	 * @throws ParseException an exception occurred during parsing
	 */
	protected abstract Expression doParseExpression(String expressionString, ParserContext context)
			throws ParseException;

}
