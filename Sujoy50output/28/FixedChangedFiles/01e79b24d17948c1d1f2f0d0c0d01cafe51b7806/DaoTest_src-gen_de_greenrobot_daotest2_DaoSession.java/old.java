package de.greenrobot.daotest2;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.DaoConfig;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.IdentityScopeType;

import de.greenrobot.daotest2.KeepEntity;

import de.greenrobot.daotest2.KeepEntityDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig keepEntityDaoConfig;

    private final KeepEntityDao keepEntityDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        keepEntityDaoConfig = daoConfigMap.get(KeepEntityDao.class).clone();
        keepEntityDaoConfig.initIdentityScope(type);

        keepEntityDao = new KeepEntityDao(keepEntityDaoConfig, this);

        registerDao(KeepEntity.class, keepEntityDao);
    }
    
    public void clear() {
        keepEntityDaoConfig.getIdentityScope().clear();
    }

    public KeepEntityDao getKeepEntityDao() {
        return keepEntityDao;
    }

}
