package de.greenrobot.testdao;


// THIS CODE IS GENERATED, DO NOT EDIT.
/** 
 * Entity mapped to table TEST_ENTITY_SIMPLE (schema version 1).
*/
public class TestEntitySimple {

    private Integer id; 
    private Integer simpleInt; 
    private int simpleIntNotNull; 
    private Long simpleLong; 
    private long simpleLongNotNull; 
    private String simpleString; 
    private String simpleStringNotNull; 
    
    public Integer getId() {
        return id;
    } 

    public void setId(Integer id) {
        this.id = id;
    } 

    public Integer getSimpleInt() {
        return simpleInt;
    } 

    public void setSimpleInt(Integer simpleInt) {
        this.simpleInt = simpleInt;
    } 

    public int getSimpleIntNotNull() {
        return simpleIntNotNull;
    } 

    public void setSimpleIntNotNull(int simpleIntNotNull) {
        this.simpleIntNotNull = simpleIntNotNull;
    } 

    public Long getSimpleLong() {
        return simpleLong;
    } 

    public void setSimpleLong(Long simpleLong) {
        this.simpleLong = simpleLong;
    } 

    public long getSimpleLongNotNull() {
        return simpleLongNotNull;
    } 

    public void setSimpleLongNotNull(long simpleLongNotNull) {
        this.simpleLongNotNull = simpleLongNotNull;
    } 

    public String getSimpleString() {
        return simpleString;
    } 

    public void setSimpleString(String simpleString) {
        this.simpleString = simpleString;
    } 

    public String getSimpleStringNotNull() {
        return simpleStringNotNull;
    } 

    public void setSimpleStringNotNull(String simpleStringNotNull) {
        this.simpleStringNotNull = simpleStringNotNull;
    } 

}
