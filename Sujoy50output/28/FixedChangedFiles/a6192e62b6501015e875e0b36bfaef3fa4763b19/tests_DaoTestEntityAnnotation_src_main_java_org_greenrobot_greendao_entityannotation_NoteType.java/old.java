package org.greenrobot.greendao.entityannotation;

public enum NoteType {
    TEXT, LIST, PICTURE
}
