package org.apache.ibatis.submitted.order_prefix_removed;

public interface PersonMapper {
    public Person select(String orderType);
}
