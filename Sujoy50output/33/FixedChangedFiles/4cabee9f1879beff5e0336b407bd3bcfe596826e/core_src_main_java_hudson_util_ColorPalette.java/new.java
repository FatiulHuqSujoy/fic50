package hudson.util;

import java.awt.Color;

/**
 * Color constants consistent with the Hudson color palette. 
 *
 * @author Kohsuke Kawaguchi
 */
public class ColorPalette {
    public static final Color RED = new Color(0xEF,0x29,0x29);
    public static final Color BLUE = new Color(0x72,0x9F,0xCF);
}
