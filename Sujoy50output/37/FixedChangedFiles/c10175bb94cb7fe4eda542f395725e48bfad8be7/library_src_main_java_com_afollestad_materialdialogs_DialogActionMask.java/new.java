package com.afollestad.materialdialogs;

/**
 * Used internally in MaterialDialog.java.
 *
 * @author Aidan Follestad (afollestad)
 */
class DialogActionMask {

    public static final int POSITIVE = 1;
    public static final int NEUTRAL = 2;
    public static final int NEGATIVE = 3;
}
