/*
 * Copyright (C) 2008 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.common.collect;
public class ArrayListMultimapTest_gwt extends com.google.gwt.junit.client.GWTTestCase {
@Override public String getModuleName() {
  return "com.google.common.collect.testModule";
}
public void testAsMap() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMap();
}

public void testAsMapEntries() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntries();
}

public void testAsMapEntriesEquals() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntriesEquals();
}

public void testAsMapEntriesToArray() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntriesToArray();
}

public void testAsMapEntriesUpdate() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntriesUpdate();
}

public void testAsMapEquals() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEquals();
}

public void testAsMapKeySetToArray() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapKeySetToArray();
}

public void testAsMapValuesRemove() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapValuesRemove();
}

public void testAsMapValuesToArray() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testAsMapValuesToArray();
}

public void testClear() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testClear();
}

public void testCreate() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testCreate();
}

public void testCreateFromArrayListMultimap() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromArrayListMultimap();
}

public void testCreateFromHashMultimap() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromHashMultimap();
}

public void testCreateFromIllegalSizes() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromIllegalSizes();
}

public void testCreateFromMultimap() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromMultimap();
}

public void testCreateFromSizes() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromSizes();
}

public void testEntries() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntries();
}

public void testEntriesCopy() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntriesCopy();
}

public void testEntriesIterator() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntriesIterator();
}

public void testEntriesRemove() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntriesRemove();
}

public void testEntriesRemoveAll() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntriesRemoveAll();
}

public void testEntriesRetainAll() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntriesRetainAll();
}

public void testEntriesUpdate() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntriesUpdate();
}

public void testEntrySetValue() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEntrySetValue();
}

public void testEqualsFalse() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEqualsFalse();
}

public void testEqualsOrdering() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEqualsOrdering();
}

public void testEqualsTrue() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testEqualsTrue();
}

public void testGetAddAll() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetAddAll();
}

public void testGetAddQuery() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetAddQuery();
}

public void testGetClear() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetClear();
}

public void testGetEquals() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetEquals();
}

public void testGetIterator() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetIterator();
}

public void testGetPutAllCollection() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetPutAllCollection();
}

public void testGetPutAllMultimap() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetPutAllMultimap();
}

public void testGetRandomAccess() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetRandomAccess();
}

public void testGetRemove() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetRemove();
}

public void testGetRemoveAddQuery() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetRemoveAddQuery();
}

public void testGetRemoveAll() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetRemoveAll();
}

public void testGetRemoveAll_someValuesRemain() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetRemoveAll_someValuesRemain();
}

public void testGetReplaceValues() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetReplaceValues();
}

public void testGetRetainAll() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testGetRetainAll();
}

public void testKeySet() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeySet();
}

public void testKeySetClear() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeySetClear();
}

public void testKeySetIterator() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeySetIterator();
}

public void testKeySetRemove() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeySetRemove();
}

public void testKeys() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeys();
}

public void testKeysClear() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeysClear();
}

public void testKeysEntrySetIterator() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeysEntrySetIterator();
}

public void testKeysEntrySetRemove() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testKeysEntrySetRemove();
}

public void testListAddIndex() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testListAddIndex();
}

public void testListEquals() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testListEquals();
}

public void testListGetSet() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testListGetSet();
}

public void testListHashCode() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testListHashCode();
}

public void testListIteratorIndexUpdate() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testListIteratorIndexUpdate();
}

public void testListPutAllIterable() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testListPutAllIterable();
}

public void testListRemoveAll() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testListRemoveAll();
}

public void testModifyCollectionFromGet() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testModifyCollectionFromGet();
}

public void testPutAllReturn_existingElements() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testPutAllReturn_existingElements();
}

public void testRemove() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testRemove();
}

public void testRemoveAll() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAll();
}

public void testRemoveAllNotPresent() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAllNotPresent();
}

public void testRemoveAllNull() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAllNull();
}

public void testRemoveAllRandomAccess() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAllRandomAccess();
}

public void testRemoveNull() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testRemoveNull();
}

public void testReplaceValuesRandomAccess() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testReplaceValuesRandomAccess();
}

public void testSublistConcurrentModificationException() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testSublistConcurrentModificationException();
}

public void testToString() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testToString();
}

public void testTrimToSize() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testTrimToSize();
}

public void testValues() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testValues();
}

public void testValuesClear() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testValuesClear();
}

public void testValuesIteratorRemove() throws Exception {
  com.google.common.collect.ArrayListMultimapTest testCase = new com.google.common.collect.ArrayListMultimapTest();
  testCase.setUp();
  testCase.testValuesIteratorRemove();
}
}
