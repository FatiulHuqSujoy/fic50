/*
 * Copyright (C) 2008 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.common.collect;
public class LinkedListMultimapTest_gwt extends com.google.gwt.junit.client.GWTTestCase {
@Override public String getModuleName() {
  return "com.google.common.collect.testModule";
}
public void testAsMap() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMap();
}

public void testAsMapEntries() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntries();
}

public void testAsMapEntriesEquals() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntriesEquals();
}

public void testAsMapEntriesToArray() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntriesToArray();
}

public void testAsMapEntriesUpdate() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEntriesUpdate();
}

public void testAsMapEquals() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapEquals();
}

public void testAsMapKeySetToArray() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapKeySetToArray();
}

public void testAsMapValuesRemove() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapValuesRemove();
}

public void testAsMapValuesToArray() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testAsMapValuesToArray();
}

public void testClear() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testClear();
}

public void testCreateFromIllegalSize() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromIllegalSize();
}

public void testCreateFromMultimap() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromMultimap();
}

public void testCreateFromSize() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testCreateFromSize();
}

public void testEntries() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntries();
}

public void testEntriesAfterMultimapUpdate() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntriesAfterMultimapUpdate();
}

public void testEntriesCopy() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntriesCopy();
}

public void testEntriesIterator() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntriesIterator();
}

public void testEntriesRemove() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntriesRemove();
}

public void testEntriesRemoveAll() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntriesRemoveAll();
}

public void testEntriesRetainAll() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntriesRetainAll();
}

public void testEntriesUpdate() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntriesUpdate();
}

public void testEntrySetValue() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEntrySetValue();
}

public void testEquals() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEquals();
}

public void testEqualsFalse() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEqualsFalse();
}

public void testEqualsOrdering() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEqualsOrdering();
}

public void testEqualsTrue() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testEqualsTrue();
}

public void testGetAddAll() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetAddAll();
}

public void testGetAddQuery() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetAddQuery();
}

public void testGetClear() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetClear();
}

public void testGetEquals() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetEquals();
}

public void testGetIterator() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetIterator();
}

public void testGetPutAllCollection() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetPutAllCollection();
}

public void testGetPutAllMultimap() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetPutAllMultimap();
}

public void testGetRandomAccess() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetRandomAccess();
}

public void testGetRemove() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetRemove();
}

public void testGetRemoveAddQuery() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetRemoveAddQuery();
}

public void testGetRemoveAll() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetRemoveAll();
}

public void testGetRemoveAll_someValuesRemain() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetRemoveAll_someValuesRemain();
}

public void testGetReplaceValues() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetReplaceValues();
}

public void testGetRetainAll() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testGetRetainAll();
}

public void testKeySet() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeySet();
}

public void testKeySetClear() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeySetClear();
}

public void testKeySetIterator() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeySetIterator();
}

public void testKeySetRemove() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeySetRemove();
}

public void testKeys() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeys();
}

public void testKeysClear() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeysClear();
}

public void testKeysEntrySetIterator() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeysEntrySetIterator();
}

public void testKeysEntrySetRemove() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testKeysEntrySetRemove();
}

public void testLinkedAsMapEntries() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedAsMapEntries();
}

public void testLinkedClear() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedClear();
}

public void testLinkedEntries() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedEntries();
}

public void testLinkedGetAdd() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedGetAdd();
}

public void testLinkedGetInsert() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedGetInsert();
}

public void testLinkedKeySet() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedKeySet();
}

public void testLinkedKeys() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedKeys();
}

public void testLinkedPutAllMultimap() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedPutAllMultimap();
}

public void testLinkedPutInOrder() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedPutInOrder();
}

public void testLinkedPutOutOfOrder() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedPutOutOfOrder();
}

public void testLinkedReplaceValues() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedReplaceValues();
}

public void testLinkedToString() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedToString();
}

public void testLinkedValues() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testLinkedValues();
}

public void testListAddIndex() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testListAddIndex();
}

public void testListEquals() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testListEquals();
}

public void testListGetSet() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testListGetSet();
}

public void testListHashCode() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testListHashCode();
}

public void testListIteratorIndexUpdate() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testListIteratorIndexUpdate();
}

public void testListPutAllIterable() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testListPutAllIterable();
}

public void testListRemoveAll() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testListRemoveAll();
}

public void testModifyCollectionFromGet() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testModifyCollectionFromGet();
}

public void testPutAllReturn_existingElements() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testPutAllReturn_existingElements();
}

public void testRemove() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testRemove();
}

public void testRemoveAll() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAll();
}

public void testRemoveAllNotPresent() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAllNotPresent();
}

public void testRemoveAllNull() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAllNull();
}

public void testRemoveAllRandomAccess() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testRemoveAllRandomAccess();
}

public void testRemoveNull() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testRemoveNull();
}

public void testReplaceValuesRandomAccess() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testReplaceValuesRandomAccess();
}

public void testToString() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testToString();
}

public void testValues() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testValues();
}

public void testValuesClear() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testValuesClear();
}

public void testValuesIteratorRemove() throws Exception {
  com.google.common.collect.LinkedListMultimapTest testCase = new com.google.common.collect.LinkedListMultimapTest();
  testCase.setUp();
  testCase.testValuesIteratorRemove();
}
}
