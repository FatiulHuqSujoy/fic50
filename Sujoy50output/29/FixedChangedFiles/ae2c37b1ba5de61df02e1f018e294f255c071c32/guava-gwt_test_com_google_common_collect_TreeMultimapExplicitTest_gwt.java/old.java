/*
 * Copyright (C) 2008 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.common.collect;
public class TreeMultimapExplicitTest_gwt extends com.google.gwt.junit.client.GWTTestCase {
@Override public String getModuleName() {
  return "com.google.common.collect.testModule";
}
public void testAsMap() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMap();
}

public void testAsMapEntries() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapEntries();
}

public void testAsMapEntriesEquals() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapEntriesEquals();
}

public void testAsMapEntriesToArray() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapEntriesToArray();
}

public void testAsMapEntriesUpdate() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapEntriesUpdate();
}

public void testAsMapEquals() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapEquals();
}

public void testAsMapKeySetToArray() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapKeySetToArray();
}

public void testAsMapToString() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapToString();
}

public void testAsMapValues() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapValues();
}

public void testAsMapValuesToArray() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testAsMapValuesToArray();
}

public void testClear() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testClear();
}

public void testComparator() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testComparator();
}

public void testDuplicates() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testDuplicates();
}

public void testEmptyGetToString() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEmptyGetToString();
}

public void testEmptyToString() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEmptyToString();
}

public void testEntries() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntries();
}

public void testEntriesCopy() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntriesCopy();
}

public void testEntriesEquals() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntriesEquals();
}

public void testEntriesIterator() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntriesIterator();
}

public void testEntriesRemove() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntriesRemove();
}

public void testEntriesRemoveAll() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntriesRemoveAll();
}

public void testEntriesRetainAll() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntriesRetainAll();
}

public void testEntriesUpdate() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntriesUpdate();
}

public void testEntrySetValue() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEntrySetValue();
}

public void testEqualsFalse() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEqualsFalse();
}

public void testEqualsTrue() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testEqualsTrue();
}

public void testGetAddAll() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetAddAll();
}

public void testGetAddQuery() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetAddQuery();
}

public void testGetClear() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetClear();
}

public void testGetComparator() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetComparator();
}

public void testGetEquals() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetEquals();
}

public void testGetIterator() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetIterator();
}

public void testGetPutAllCollection() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetPutAllCollection();
}

public void testGetPutAllMultimap() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetPutAllMultimap();
}

public void testGetRemove() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetRemove();
}

public void testGetRemoveAddQuery() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetRemoveAddQuery();
}

public void testGetRemoveAll() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetRemoveAll();
}

public void testGetRemoveToString() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetRemoveToString();
}

public void testGetReplaceValues() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testGetReplaceValues();
}

public void testKeySet() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeySet();
}

public void testKeySetClear() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeySetClear();
}

public void testKeySetIterator() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeySetIterator();
}

public void testKeySetRemove() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeySetRemove();
}

public void testKeys() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeys();
}

public void testKeysClear() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeysClear();
}

public void testKeysEntrySetIterator() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeysEntrySetIterator();
}

public void testKeysEntrySetRemove() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testKeysEntrySetRemove();
}

public void testModifyCollectionFromGet() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testModifyCollectionFromGet();
}

public void testMultimapComparators() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testMultimapComparators();
}

public void testMultimapCreateFromTreeMultimap() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testMultimapCreateFromTreeMultimap();
}

public void testOrderedAsMapEntries() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testOrderedAsMapEntries();
}

public void testOrderedEntries() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testOrderedEntries();
}

public void testOrderedGet() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testOrderedGet();
}

public void testOrderedKeySet() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testOrderedKeySet();
}

public void testOrderedValues() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testOrderedValues();
}

public void testPutAllReturn_existingElements() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testPutAllReturn_existingElements();
}

public void testPutReturn() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testPutReturn();
}

public void testRemove() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testRemove();
}

public void testRemoveAll() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testRemoveAll();
}

public void testRemoveAllNotPresent() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testRemoveAllNotPresent();
}

public void testRemoveAllNull() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testRemoveAllNull();
}

public void testRemoveNull() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testRemoveNull();
}

public void testRemoveToString() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testRemoveToString();
}

public void testSortedKeySet() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testSortedKeySet();
}

public void testToString() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testToString();
}

public void testToStringNull() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testToStringNull();
}

public void testValues() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testValues();
}

public void testValuesClear() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testValuesClear();
}

public void testValuesIteratorRemove() throws Exception {
  com.google.common.collect.TreeMultimapExplicitTest testCase = new com.google.common.collect.TreeMultimapExplicitTest();
  testCase.setUp();
  testCase.testValuesIteratorRemove();
}
}
