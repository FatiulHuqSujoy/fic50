/*
 * Copyright (C) 2008 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.common.collect;
public class MultimapsTest_gwt extends com.google.gwt.junit.client.GWTTestCase {
@Override public String getModuleName() {
  return "com.google.common.collect.testModule";
}
public void testAsMap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMap();
}

public void testAsMapEntries() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMapEntries();
}

public void testAsMapEntriesToArray() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMapEntriesToArray();
}

public void testAsMapEntriesUpdate() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMapEntriesUpdate();
}

public void testAsMapKeySetToArray() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMapKeySetToArray();
}

public void testAsMapToString() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMapToString();
}

public void testAsMapValuesToArray() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMapValuesToArray();
}

public void testAsMap_listMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMap_listMultimap();
}

public void testAsMap_multimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMap_multimap();
}

public void testAsMap_setMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMap_setMultimap();
}

public void testAsMap_sortedSetMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testAsMap_sortedSetMultimap();
}

public void testClear() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testClear();
}

public void testEmptyGetToString() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEmptyGetToString();
}

public void testEmptyToString() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEmptyToString();
}

public void testEntries() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntries();
}

public void testEntriesCopy() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntriesCopy();
}

public void testEntriesIterator() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntriesIterator();
}

public void testEntriesRemove() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntriesRemove();
}

public void testEntriesRemoveAll() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntriesRemoveAll();
}

public void testEntriesRetainAll() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntriesRetainAll();
}

public void testEntriesUpdate() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntriesUpdate();
}

public void testEntrySetValue() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEntrySetValue();
}

public void testEqualsFalse() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEqualsFalse();
}

public void testEqualsTrue() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testEqualsTrue();
}

public void testFilteredKeysListMultimapGetBadValue() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testFilteredKeysListMultimapGetBadValue();
}

public void testFilteredKeysSetMultimapGetBadValue() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testFilteredKeysSetMultimapGetBadValue();
}

public void testFilteredKeysSetMultimapReplaceValues() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testFilteredKeysSetMultimapReplaceValues();
}

public void testForMap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testForMap();
}

public void testForMapAsMap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testForMapAsMap();
}

public void testForMapGetIteration() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testForMapGetIteration();
}

public void testForMapRemoveAll() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testForMapRemoveAll();
}

public void testGetAddAll() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetAddAll();
}

public void testGetAddQuery() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetAddQuery();
}

public void testGetClear() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetClear();
}

public void testGetIterator() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetIterator();
}

public void testGetPutAllCollection() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetPutAllCollection();
}

public void testGetPutAllMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetPutAllMultimap();
}

public void testGetRemove() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetRemove();
}

public void testGetRemoveAddQuery() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetRemoveAddQuery();
}

public void testGetRemoveAll() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetRemoveAll();
}

public void testGetRemoveToString() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetRemoveToString();
}

public void testGetReplaceValues() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testGetReplaceValues();
}

public void testIndex() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testIndex();
}

public void testIndexIterator() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testIndexIterator();
}

public void testIndex_nullKey() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testIndex_nullKey();
}

public void testIndex_nullValue() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testIndex_nullValue();
}

public void testIndex_ordering() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testIndex_ordering();
}

public void testInvertFrom() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testInvertFrom();
}

public void testKeySet() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeySet();
}

public void testKeySetClear() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeySetClear();
}

public void testKeySetIterator() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeySetIterator();
}

public void testKeySetRemove() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeySetRemove();
}

public void testKeys() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeys();
}

public void testKeysClear() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeysClear();
}

public void testKeysEntrySetIterator() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeysEntrySetIterator();
}

public void testKeysEntrySetRemove() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testKeysEntrySetRemove();
}

public void testModifyCollectionFromGet() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testModifyCollectionFromGet();
}

public void testNewListMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testNewListMultimap();
}

public void testNewMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testNewMultimap();
}

public void testNewMultimapWithCollectionRejectingNegativeElements() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testNewMultimapWithCollectionRejectingNegativeElements();
}

public void testNewSetMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testNewSetMultimap();
}

public void testNewSortedSetMultimap() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testNewSortedSetMultimap();
}

public void testRemove() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testRemove();
}

public void testRemoveAll() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testRemoveAll();
}

public void testRemoveAllNotPresent() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testRemoveAllNotPresent();
}

public void testRemoveAllNull() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testRemoveAllNull();
}

public void testRemoveNull() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testRemoveNull();
}

public void testRemoveToString() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testRemoveToString();
}

public void testSynchronizedMultimapSampleCodeCompilation() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testSynchronizedMultimapSampleCodeCompilation();
}

public void testToStringNull() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testToStringNull();
}

public void testUnmodifiableArrayListMultimapRandomAccess() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testUnmodifiableArrayListMultimapRandomAccess();
}

public void testUnmodifiableLinkedListMultimapRandomAccess() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testUnmodifiableLinkedListMultimapRandomAccess();
}

public void testUnmodifiableListMultimapShortCircuit() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testUnmodifiableListMultimapShortCircuit();
}

public void testUnmodifiableMultimapEntries() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testUnmodifiableMultimapEntries();
}

public void testUnmodifiableMultimapIsView() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testUnmodifiableMultimapIsView();
}

public void testUnmodifiableMultimapShortCircuit() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testUnmodifiableMultimapShortCircuit();
}

public void testUnmodifiableSetMultimapShortCircuit() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testUnmodifiableSetMultimapShortCircuit();
}

public void testValues() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testValues();
}

public void testValuesClear() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testValuesClear();
}

public void testValuesIteratorRemove() throws Exception {
  com.google.common.collect.MultimapsTest testCase = new com.google.common.collect.MultimapsTest();
  testCase.setUp();
  testCase.testValuesIteratorRemove();
}
}
