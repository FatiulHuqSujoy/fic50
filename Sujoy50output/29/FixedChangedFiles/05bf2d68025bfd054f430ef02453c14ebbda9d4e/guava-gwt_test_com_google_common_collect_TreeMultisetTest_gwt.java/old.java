/*
 * Copyright (C) 2008 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.common.collect;
public class TreeMultisetTest_gwt extends com.google.gwt.junit.client.GWTTestCase {
@Override public String getModuleName() {
  return "com.google.common.collect.testModule";
}
public void testAddOne() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testAddOne();
}

public void testAddSeveralTimes() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testAddSeveralTimes();
}

public void testClear() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testClear();
}

public void testClearNothing() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testClearNothing();
}

public void testClearViaElementSet() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testClearViaElementSet();
}

public void testClearViaEntrySet() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testClearViaEntrySet();
}

public void testContainsAfterRemoval() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testContainsAfterRemoval();
}

public void testContainsAllVacuous() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testContainsAllVacuous();
}

public void testContainsNo() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testContainsNo();
}

public void testContainsOne() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testContainsOne();
}

public void testCreate() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testCreate();
}

public void testCreateFromIterable() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testCreateFromIterable();
}

public void testCreateWithComparator() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testCreateWithComparator();
}

public void testCustomComparator() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testCustomComparator();
}

public void testDegenerateComparator() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testDegenerateComparator();
}

public void testElementSetIsNotACopy() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testElementSetIsNotACopy();
}

public void testElementSetSortedSetMethods() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testElementSetSortedSetMethods();
}

public void testElementSetSubsetClear() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testElementSetSubsetClear();
}

public void testElementSetSubsetRemove() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testElementSetSubsetRemove();
}

public void testElementSetSubsetRemoveAll() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testElementSetSubsetRemoveAll();
}

public void testElementSetSubsetRetainAll() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testElementSetSubsetRetainAll();
}

public void testEntryAfterClear() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterClear();
}

public void testEntryAfterElementSetIteratorRemove() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterElementSetIteratorRemove();
}

public void testEntryAfterEntrySetClear() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterEntrySetClear();
}

public void testEntryAfterEntrySetIteratorRemove() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterEntrySetIteratorRemove();
}

public void testEntryAfterRemove() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterRemove();
}

public void testEntrySetRemove() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEntrySetRemove();
}

public void testEqualsDifferentTypes() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEqualsDifferentTypes();
}

public void testEqualsNo() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEqualsNo();
}

public void testEqualsPartial() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEqualsPartial();
}

public void testEqualsSelf() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEqualsSelf();
}

public void testEqualsTricky() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEqualsTricky();
}

public void testEqualsYes() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testEqualsYes();
}

public void testIsEmptyNo() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testIsEmptyNo();
}

public void testIsEmptyYes() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testIsEmptyYes();
}

public void testNullAcceptingComparator() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testNullAcceptingComparator();
}

public void testReallyBig() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testReallyBig();
}

public void testRemoveAllVacuous() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testRemoveAllVacuous();
}

public void testRemoveFromElementSetNo() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testRemoveFromElementSetNo();
}

public void testRemoveFromElementSetYes() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testRemoveFromElementSetYes();
}

public void testRemoveOneFromNoneStandard() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testRemoveOneFromNoneStandard();
}

public void testRemoveOneFromOneStandard() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testRemoveOneFromOneStandard();
}

public void testRetainAllOfNothing() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testRetainAllOfNothing();
}

public void testRetainAllVacuous() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testRetainAllVacuous();
}

public void testSubMultisetSize() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testSubMultisetSize();
}

public void testToArrayOne() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testToArrayOne();
}

public void testToString() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testToString();
}

public void testToStringNull() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testToStringNull();
}

public void testUnmodifiableMultiset() throws Exception {
  com.google.common.collect.TreeMultisetTest testCase = new com.google.common.collect.TreeMultisetTest();
  testCase.setUp();
  testCase.testUnmodifiableMultiset();
}
}
