/*
 * Copyright (C) 2008 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.common.collect;
public class LinkedHashMultisetTest_gwt extends com.google.gwt.junit.client.GWTTestCase {
@Override public String getModuleName() {
  return "com.google.common.collect.testModule";
}
public void testAddOne() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testAddOne();
}

public void testAddSeveralTimes() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testAddSeveralTimes();
}

public void testClear() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testClear();
}

public void testClearNothing() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testClearNothing();
}

public void testClearViaElementSet() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testClearViaElementSet();
}

public void testClearViaEntrySet() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testClearViaEntrySet();
}

public void testContainsAfterRemoval() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testContainsAfterRemoval();
}

public void testContainsAllVacuous() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testContainsAllVacuous();
}

public void testContainsNo() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testContainsNo();
}

public void testContainsOne() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testContainsOne();
}

public void testCreate() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testCreate();
}

public void testCreateFromIterable() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testCreateFromIterable();
}

public void testCreateWithSize() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testCreateWithSize();
}

public void testElementSetIsNotACopy() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testElementSetIsNotACopy();
}

public void testEntryAfterClear() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterClear();
}

public void testEntryAfterElementSetIteratorRemove() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterElementSetIteratorRemove();
}

public void testEntryAfterEntrySetClear() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterEntrySetClear();
}

public void testEntryAfterEntrySetIteratorRemove() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterEntrySetIteratorRemove();
}

public void testEntryAfterRemove() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEntryAfterRemove();
}

public void testEntrySetRemove() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEntrySetRemove();
}

public void testEqualsDifferentTypes() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEqualsDifferentTypes();
}

public void testEqualsNo() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEqualsNo();
}

public void testEqualsPartial() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEqualsPartial();
}

public void testEqualsSelf() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEqualsSelf();
}

public void testEqualsTricky() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEqualsTricky();
}

public void testEqualsYes() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testEqualsYes();
}

public void testIsEmptyNo() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testIsEmptyNo();
}

public void testIsEmptyYes() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testIsEmptyYes();
}

public void testIteratorRemoveConcurrentModification() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testIteratorRemoveConcurrentModification();
}

public void testLosesPlaceInLine() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testLosesPlaceInLine();
}

public void testReallyBig() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testReallyBig();
}

public void testRemoveAllVacuous() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testRemoveAllVacuous();
}

public void testRemoveFromElementSetNo() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testRemoveFromElementSetNo();
}

public void testRemoveFromElementSetYes() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testRemoveFromElementSetYes();
}

public void testRemoveOneFromNoneStandard() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testRemoveOneFromNoneStandard();
}

public void testRemoveOneFromOneStandard() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testRemoveOneFromOneStandard();
}

public void testRetainAllOfNothing() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testRetainAllOfNothing();
}

public void testRetainAllVacuous() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testRetainAllVacuous();
}

public void testToArrayOne() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testToArrayOne();
}

public void testToString() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testToString();
}

public void testToStringNull() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testToStringNull();
}

public void testUnmodifiableMultiset() throws Exception {
  com.google.common.collect.LinkedHashMultisetTest testCase = new com.google.common.collect.LinkedHashMultisetTest();
  testCase.setUp();
  testCase.testUnmodifiableMultiset();
}
}
