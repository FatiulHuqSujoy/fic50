/*
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.devtools.j2objc.translate;

import com.google.common.collect.Lists;
import com.google.devtools.j2objc.types.IOSMethodBinding;
import com.google.devtools.j2objc.types.NodeCopier;
import com.google.devtools.j2objc.types.Types;
import com.google.devtools.j2objc.util.ASTUtil;
import com.google.devtools.j2objc.util.ErrorReportingASTVisitor;
import com.google.devtools.j2objc.util.NameTable;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.WhileStatement;

import java.util.List;

/**
 * Adds support for boxing and unboxing numeric primitive values.
 *
 * @author Tom Ball
 */
public class Autoboxer extends ErrorReportingASTVisitor {
  private final AST ast;
  private static final String VALUE_METHOD = "Value";
  private static final String VALUEOF_METHOD = "valueOf";

  public Autoboxer(AST ast) {
    this.ast = ast;
  }

  /**
   * Convert a primitive type expression into a wrapped instance.  Each
   * wrapper class has a static valueOf factory method, so "expr" gets
   * translated to "Wrapper.valueOf(expr)".
   */
  private Expression box(Expression expr) {
    ITypeBinding binding = getBoxType(expr);
    ITypeBinding wrapperBinding = Types.getWrapperType(binding);
    if (wrapperBinding != null) {
      MethodInvocation invocation = ast.newMethodInvocation();
      SimpleName wrapperClass = ast.newSimpleName(wrapperBinding.getName());
      Types.addBinding(wrapperClass, wrapperBinding);
      invocation.setExpression(wrapperClass);
      IMethodBinding wrapperMethod = getWrapperMethod(wrapperBinding, binding);
      SimpleName methodName = ast.newSimpleName(VALUEOF_METHOD);
      Types.addBinding(methodName, wrapperMethod);
      invocation.setName(methodName);
      Types.addBinding(invocation, wrapperMethod);

      ASTUtil.getArguments(invocation).add(NodeCopier.copySubtree(ast, expr));
      return invocation;
    } else {
      return NodeCopier.copySubtree(ast, expr);
    }
  }

  /**
   * Convert a wrapper class instance to its primitive equivalent.  Each
   * wrapper class has a "classValue()" method, such as intValue() or
   * booleanValue().  This method therefore converts "expr" to
   * "expr.classValue()".
   */
  private Expression unbox(Expression expr) {
    ITypeBinding binding = getBoxType(expr);
    if (Types.getPrimitiveType(binding) != null) {
      IMethodBinding valueMethod = getValueMethod(binding);
      MethodInvocation invocation = ast.newMethodInvocation();
      invocation.setExpression(NodeCopier.copySubtree(ast, expr));
      SimpleName methodName = ast.newSimpleName(valueMethod.getName());
      Types.addBinding(methodName, valueMethod);
      invocation.setName(methodName);
      Types.addBinding(invocation, valueMethod);
      return invocation;
    } else {
      return NodeCopier.copySubtree(ast, expr);
    }
  }

  @Override
  public void endVisit(Assignment node) {
    Expression lhs = node.getLeftHandSide();
    ITypeBinding lhType = getBoxType(lhs);
    Expression rhs = node.getRightHandSide();
    ITypeBinding rhType = getBoxType(rhs);
    Assignment.Operator op = node.getOperator();
    if (op != Assignment.Operator.ASSIGN && !lhType.isPrimitive() &&
        !lhType.equals(node.getAST().resolveWellKnownType("java.lang.String"))) {
      // Not a simple assignment; need to break the <operation>-WITH_ASSIGN
      // assignment apart.
      node.setOperator(Assignment.Operator.ASSIGN);
      node.setRightHandSide(box(newInfixExpression(lhs, rhs, op, lhType)));
    } else {
      if (lhType.isPrimitive() && !rhType.isPrimitive()) {
        node.setRightHandSide(unbox(rhs));
      } else if (!lhType.isPrimitive() && rhType.isPrimitive()) {
        node.setRightHandSide(box(rhs));
      }
    }
  }

  private InfixExpression newInfixExpression(
      Expression lhs, Expression rhs, Assignment.Operator op, ITypeBinding lhType) {
    InfixExpression newRhs = ast.newInfixExpression();
    newRhs.setLeftOperand(unbox(lhs));
    newRhs.setRightOperand(unbox(rhs));
    InfixExpression.Operator infixOp;
    // op isn't an enum, so this can't be a switch.
    if (op == Assignment.Operator.PLUS_ASSIGN) {
      infixOp = InfixExpression.Operator.PLUS;
    } else if (op == Assignment.Operator.MINUS_ASSIGN) {
      infixOp = InfixExpression.Operator.MINUS;
    } else if (op == Assignment.Operator.TIMES_ASSIGN) {
      infixOp = InfixExpression.Operator.TIMES;
    } else if (op == Assignment.Operator.DIVIDE_ASSIGN) {
      infixOp = InfixExpression.Operator.DIVIDE;
    } else if (op == Assignment.Operator.BIT_AND_ASSIGN) {
      infixOp = InfixExpression.Operator.AND;
    } else if (op == Assignment.Operator.BIT_OR_ASSIGN) {
      infixOp = InfixExpression.Operator.OR;
    } else if (op == Assignment.Operator.BIT_XOR_ASSIGN) {
      infixOp = InfixExpression.Operator.XOR;
    } else if (op == Assignment.Operator.REMAINDER_ASSIGN) {
      infixOp = InfixExpression.Operator.REMAINDER;
    } else if (op == Assignment.Operator.LEFT_SHIFT_ASSIGN) {
      infixOp = InfixExpression.Operator.LEFT_SHIFT;
    } else if (op == Assignment.Operator.RIGHT_SHIFT_SIGNED_ASSIGN) {
      infixOp = InfixExpression.Operator.RIGHT_SHIFT_SIGNED;
    } else if (op == Assignment.Operator.RIGHT_SHIFT_UNSIGNED_ASSIGN) {
      infixOp = InfixExpression.Operator.RIGHT_SHIFT_UNSIGNED;
    } else {
      throw new IllegalArgumentException();
    }
    newRhs.setOperator(infixOp);
    Types.addBinding(newRhs, Types.getPrimitiveType(lhType));
    return newRhs;
  }

  @Override
  public void endVisit(ArrayAccess node) {
    Expression index = node.getIndex();
    if (!getBoxType(index).isPrimitive()) {
      node.setIndex(unbox(index));
    }
  }

  @Override
  public void endVisit(ArrayInitializer node) {
    ITypeBinding type = Types.getTypeBinding(node).getElementType();
    List<Expression> expressions = ASTUtil.getExpressions(node);
    for (int i = 0; i < expressions.size(); i++) {
      Expression expr = expressions.get(i);
      Expression result = boxOrUnboxExpression(expr, type);
      if (expr != result) {
        expressions.set(i, result);
      }
    }
  }

  @Override
  public void endVisit(CastExpression node) {
    Expression expr = boxOrUnboxExpression(node.getExpression(), Types.getTypeBinding(node));
    if (expr != node.getExpression()) {
      ASTNode parent = node.getParent();
      if (parent instanceof Expression) {
        // Check if this cast is an argument or the invocation's expression.
        if (parent instanceof MethodInvocation) {
          List<Expression> args = ASTUtil.getArguments((MethodInvocation) parent);
          for (int i = 0; i < args.size(); i++) {
            if (node.equals(args.get(i))) {
              args.set(i, expr);
              return;
            }
          }
        }
        ASTUtil.setProperty(node.getParent(), expr);
      }
    }
  }

  @Override
  public void endVisit(ClassInstanceCreation node) {
    convertArguments(Types.getMethodBinding(node), ASTUtil.getArguments(node));
  }

  @Override
  public void endVisit(ConditionalExpression node) {
    ITypeBinding nodeType = getBoxType(node);

    Expression thenExpr = node.getThenExpression();
    ITypeBinding thenType = getBoxType(thenExpr);

    Expression elseExpr = node.getElseExpression();
    ITypeBinding elseType = getBoxType(elseExpr);

    if (thenType.isPrimitive() && !nodeType.isPrimitive()) {
      node.setThenExpression(box(thenExpr));
    } else if (!thenType.isPrimitive() && nodeType.isPrimitive()) {
      node.setThenExpression(unbox(thenExpr));
    }

    if (elseType.isPrimitive() && !nodeType.isPrimitive()) {
      node.setElseExpression(box(elseExpr));
    } else if (!elseType.isPrimitive() && nodeType.isPrimitive()) {
      node.setElseExpression(unbox(elseExpr));
    }
  }

  @Override
  public void endVisit(ConstructorInvocation node) {
    convertArguments(Types.getMethodBinding(node), ASTUtil.getArguments(node));
  }

  @Override
  public void endVisit(DoStatement node) {
    Expression expression = node.getExpression();
    ITypeBinding exprType = getBoxType(expression);
    if (!exprType.isPrimitive()) {
      node.setExpression(unbox(expression));
    }
  }

  @Override
  public void endVisit(EnumConstantDeclaration node) {
    convertArguments(Types.getMethodBinding(node), ASTUtil.getArguments(node));
  }

  @Override
  public void endVisit(IfStatement node) {
    Expression expr = node.getExpression();
    ITypeBinding binding = getBoxType(expr);

    if (!binding.isPrimitive()) {
      node.setExpression(unbox(expr));
    }
  }

  @Override
  public void endVisit(InfixExpression node) {
    ITypeBinding type = Types.getTypeBinding(node);
    Expression lhs = node.getLeftOperand();
    ITypeBinding lhBinding = getBoxType(lhs);
    Expression rhs = node.getRightOperand();
    ITypeBinding rhBinding = getBoxType(rhs);
    InfixExpression.Operator op = node.getOperator();

    // Don't unbox for equality tests where both operands are boxed types.
    if ((op == InfixExpression.Operator.EQUALS || op == InfixExpression.Operator.NOT_EQUALS)
        && !lhBinding.isPrimitive() && !rhBinding.isPrimitive()) {
      return;
    }
    // Don't unbox for string concatenation.
    if (op == InfixExpression.Operator.PLUS && Types.isJavaStringType(type)) {
      return;
    }

    if (!lhBinding.isPrimitive()) {
      node.setLeftOperand(unbox(lhs));
    }
    if (!rhBinding.isPrimitive()) {
      node.setRightOperand(unbox(rhs));
    }
    List<Expression> extendedOperands = ASTUtil.getExtendedOperands(node);
    for (int i = 0; i < extendedOperands.size(); i++) {
      Expression expr = extendedOperands.get(i);
      if (!getBoxType(expr).isPrimitive()) {
        extendedOperands.set(i, unbox(expr));
      }
    }
  }

  @Override
  public void endVisit(MethodInvocation node) {
    convertArguments(Types.getMethodBinding(node), ASTUtil.getArguments(node));
  }

  @Override
  public void endVisit(PrefixExpression node) {
    PrefixExpression.Operator op = node.getOperator();
    Expression operand = node.getOperand();
    if (op == PrefixExpression.Operator.INCREMENT) {
      rewriteBoxedPrefixOrPostfix(node, operand, "PreIncr");
    } else if (op == PrefixExpression.Operator.DECREMENT) {
      rewriteBoxedPrefixOrPostfix(node, operand, "PreDecr");
    } else if (!getBoxType(operand).isPrimitive()) {
      node.setOperand(unbox(operand));
    }
  }

  @Override
  public void endVisit(PostfixExpression node) {
    PostfixExpression.Operator op = node.getOperator();
    if (op == PostfixExpression.Operator.INCREMENT) {
      rewriteBoxedPrefixOrPostfix(node, node.getOperand(), "PostIncr");
    } else if (op == PostfixExpression.Operator.DECREMENT) {
      rewriteBoxedPrefixOrPostfix(node, node.getOperand(), "PostDecr");
    }
  }

  private void rewriteBoxedPrefixOrPostfix(
      ASTNode node, Expression operand, String methodPrefix) {
    ITypeBinding type = getBoxType(operand);
    if (!Types.isBoxedPrimitive(type)) {
      return;
    }
    AST ast = node.getAST();
    String methodName = methodPrefix + NameTable.capitalize(Types.getPrimitiveType(type).getName());
    IOSMethodBinding methodBinding = IOSMethodBinding.newFunction(methodName, type, type, type);
    MethodInvocation invocation = ASTFactory.newMethodInvocation(ast, methodBinding, null);
    ASTUtil.getArguments(invocation).add(
        ASTFactory.newAddressOf(ast, NodeCopier.copySubtree(ast, operand)));
    ASTUtil.setProperty(node, invocation);
  }

  @Override
  public void endVisit(ReturnStatement node) {
    Expression expr = node.getExpression();
    if (expr != null) {
      ASTNode n = node.getParent();
      while (!(n instanceof MethodDeclaration)) {
        n = n.getParent();
      }
      ITypeBinding returnType = Types.getMethodBinding(n).getReturnType();
      ITypeBinding exprType = getBoxType(expr);
      if (returnType.isPrimitive() && !exprType.isPrimitive()) {
        node.setExpression(unbox(expr));
      }
      if (!returnType.isPrimitive() && exprType.isPrimitive()) {
        node.setExpression(box(expr));
      }
    }
  }

  @Override
  public void endVisit(SuperConstructorInvocation node) {
    convertArguments(Types.getMethodBinding(node), ASTUtil.getArguments(node));
  }

  @Override
  public void endVisit(SuperMethodInvocation node) {
    convertArguments(Types.getMethodBinding(node), ASTUtil.getArguments(node));
  }

  @Override
  public void endVisit(VariableDeclarationFragment node) {
    Expression initializer = node.getInitializer();
    if (initializer != null) {
      ITypeBinding nodeType = getBoxType(node);
      ITypeBinding initType = getBoxType(initializer);
      if (nodeType.isPrimitive() && !initType.isPrimitive()) {
        node.setInitializer(unbox(initializer));
      } else if (!nodeType.isPrimitive() && initType.isPrimitive()) {
        node.setInitializer(box(initializer));
      }
    }
  }

  @Override
  public void endVisit(WhileStatement node) {
    Expression expression = node.getExpression();
    ITypeBinding exprType = getBoxType(expression);
    if (!exprType.isPrimitive()) {
      node.setExpression(unbox(expression));
    }
  }

  @Override
  public void endVisit(SwitchStatement node) {
    Expression expression = node.getExpression();
    ITypeBinding exprType = getBoxType(expression);
    if (!exprType.isPrimitive()) {
      node.setExpression(unbox(expression));
    }
  }

  /**
   * Return the type to be checked for boxing or unboxing.
   */
  private ITypeBinding getBoxType(ASTNode node) {
    IBinding binding = Types.getBinding(node);
    if (binding instanceof ITypeBinding) {
      return (ITypeBinding) binding;
    }
    if (binding instanceof IVariableBinding) {
      return ((IVariableBinding) binding).getType();
    }
    if (binding instanceof IMethodBinding) {
      IMethodBinding method = (IMethodBinding) binding;
      return method.isConstructor() ? method.getDeclaringClass() : method.getReturnType();
    }
    throw new AssertionError("unknown box type");
  }

  private IMethodBinding getWrapperMethod(ITypeBinding wrapperClass, ITypeBinding primitiveType) {
    for (IMethodBinding method : wrapperClass.getDeclaredMethods()) {
      if (method.getName().equals(VALUEOF_METHOD)
          && method.getParameterTypes()[0].isEqualTo(primitiveType)) {
        return method;
      }
    }
    throw new AssertionError("could not find valueOf method for " + wrapperClass);
  }

  private IMethodBinding getValueMethod(ITypeBinding type) {
    ITypeBinding primitiveType = Types.getPrimitiveType(type);
    assert primitiveType != null;
    String methodName = primitiveType.getName() + VALUE_METHOD;
    for (IMethodBinding method : type.getDeclaredMethods()) {
      if (method.getName().equals(methodName)) {
        return method;
      }
    }
    throw new AssertionError("could not find value method for " + type);
  }

  private void convertArguments(IMethodBinding methodBinding, List<Expression> args) {
    if (methodBinding instanceof IOSMethodBinding) {
      return; // already converted
    }
    if (Types.isJavaStringType(methodBinding.getDeclaringClass()) &&
        methodBinding.getName().equals("format")) {
      // The String.format methods are mapped directly to NSString methods,
      // but arguments referred to as objects by the format string need to
      // be boxed.
      if (!args.isEmpty()) {
        Expression first = args.get(0);
        int firstArg = 1;
        ITypeBinding typeBinding = Types.getTypeBinding(first);
        if (typeBinding.getQualifiedName().equals("java.util.Locale")) {
          first = args.get(1);
          firstArg = 2;
          typeBinding = Types.getTypeBinding(first);
        }
        if (first instanceof StringLiteral) {
          List<Boolean> formatSpecifiers =
              getFormatSpecifiers(((StringLiteral) first).getLiteralValue());
          for (int i = 0; i < formatSpecifiers.size(); i++) {
            if (formatSpecifiers.get(i)) {
              int iArg = i + firstArg;
              Expression arg = args.get(iArg);
              if (Types.getTypeBinding(arg).isPrimitive()) {
                args.set(iArg, box(arg));
              }
            }
          }
        }
      }
      return;
    }

    ITypeBinding[] paramTypes = methodBinding.getParameterTypes();
    for (int i = 0; i < args.size(); i++) {
      ITypeBinding paramType;
      if (methodBinding.isVarargs() && i >= paramTypes.length - 1) {
        paramType = paramTypes[paramTypes.length - 1].getComponentType();
      } else {
        paramType = paramTypes[i];
      }
      Expression arg = args.get(i);
      Expression replacementArg = boxOrUnboxExpression(arg, paramType);
      if (replacementArg != arg) {
        args.set(i, replacementArg);
      }
    }
  }

  // Returns a list of booleans indicating whether the format specifier
  // refers to an object argument or not.
  private List<Boolean> getFormatSpecifiers(String format) {
    List<Boolean> specifiers = Lists.newArrayList();
    int i = 0;
    while ((i = format.indexOf('%', i)) != -1) {
      if (format.charAt(++i) == '%') {
        // Skip %% sequences, since they don't have associated args.
        ++i;
        continue;
      }
      specifiers.add(format.charAt(i) == '@');
    }
    return specifiers;
  }

  private Expression boxOrUnboxExpression(Expression arg, ITypeBinding argType) {
    ITypeBinding argBinding;
    IBinding binding = Types.getBinding(arg);
    if (binding instanceof IMethodBinding) {
      argBinding = ((IMethodBinding) binding).getReturnType();
    } else {
      argBinding = getBoxType(arg);
    }
    if (argType.isPrimitive() && !argBinding.isPrimitive()) {
      return unbox(arg);
    } else if (!argType.isPrimitive() && argBinding.isPrimitive()) {
      return box(arg);
    } else {
      return arg;
    }
  }
}
