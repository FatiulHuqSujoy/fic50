/**
 * 
 */
package org.mockito.internal.returnvalues;

interface HasPrimitiveMethods {
    boolean booleanMethod();
    char charMethod();
    int intMethod();
    long longMethod();
    float floatMethod();
    double doubleMethod();
}
