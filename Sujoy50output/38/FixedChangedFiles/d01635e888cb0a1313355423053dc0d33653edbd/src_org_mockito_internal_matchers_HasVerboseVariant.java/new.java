package org.mockito.internal.matchers;

import org.hamcrest.SelfDescribing;

public interface HasVerboseVariant {
    SelfDescribing getVerboseVariant();
}
