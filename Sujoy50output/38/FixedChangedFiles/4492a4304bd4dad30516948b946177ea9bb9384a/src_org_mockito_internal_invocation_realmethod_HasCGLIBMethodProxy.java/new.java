package org.mockito.internal.invocation.realmethod;

import org.mockito.cglib.proxy.MethodProxy;

public interface HasCGLIBMethodProxy {

    MethodProxy getMethodProxy();
}
