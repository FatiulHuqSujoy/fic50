package org.mockito.internal.runners;

public interface TestCreationListener {

    void testCreated(Object test);

}
