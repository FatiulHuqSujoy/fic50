package org.mockito.usage.binding;

public class SuperClassUseSuperDerived extends BaseClass<Message, IDerivedInterface> {
	
	public SuperClassUseSuperDerived(IDerivedInterface derivedIterfaceObject) {
		super(derivedIterfaceObject);
	}

}
