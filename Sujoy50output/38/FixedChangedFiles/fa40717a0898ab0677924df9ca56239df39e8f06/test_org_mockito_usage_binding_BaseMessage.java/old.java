package org.mockito.usage.binding;

public class BaseMessage {
	private String content;

	public String getContent() {
		return content;
	}

	protected void setContent(String content) {
		this.content = content;
	}
}
