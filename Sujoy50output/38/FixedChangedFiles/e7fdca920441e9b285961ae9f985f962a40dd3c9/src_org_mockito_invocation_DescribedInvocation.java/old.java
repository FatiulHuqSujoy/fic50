package org.mockito.invocation;

/**
 * by Szczepan Faber, created at: 4/1/12
 */
public interface DescribedInvocation {

    String toString();

    Location getLocation();
}
