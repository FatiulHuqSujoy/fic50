package org.mockito.internal.verification;

import org.mockito.internal.verification.api.VerificationData;
import org.mockito.internal.verification.api.VerificationMode;

public class DummyVerificationMode implements VerificationMode {
    public void verify(VerificationData data) {
    }
}
