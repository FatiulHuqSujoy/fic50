<?xml version="1.0" encoding="UTF-8"?>
<local-project language="java" version="3.5.4164" flavor="j2se">
  <property name="show-needs-to-compile" value="false" />
  <property name="hide-deprecated" value="false" />
  <property name="parse-archive-in-archive" value="false" />
  <property name="hide-externals" value="true" />
  <property name="detail-mode" value="true" />
  <property name="project-type" value="maven" />
  <property name="action-set-mod" value="10" />
  <classpath relativeto="/Users/pwebb/projects/spring/spring-bootstrap/code">
    <classpathentry kind="lib" path="spring-zero-maven-plugin/target/spring-zero-maven-plugin-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-autoconfigure/target/spring-zero-autoconfigure-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-actuator/target/spring-zero-starter-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-security/target/spring-zero-starter-security-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-core/target/spring-zero-core-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-batch/target/spring-zero-starter-batch-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-web/target/spring-zero-starter-web-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-launcher/target/spring-zero-launcher-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-cli/target/spring-zero-cli-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-integration/target/spring-zero-starter-integration-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-data-jpa/target/spring-zero-starter-data-jpa-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter/target/spring-zero-starter-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-actuator/target/spring-zero-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-maven-plugin/target/spring-zero-maven-plugin-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-autoconfigure/target/spring-zero-autoconfigure-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-actuator/target/spring-zero-starter-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-security/target/spring-zero-starter-security-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-core/target/spring-zero-core-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-batch/target/spring-zero-starter-batch-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-web/target/spring-zero-starter-web-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-launcher/target/spring-zero-launcher-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-cli/target/spring-zero-cli-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-integration/target/spring-zero-starter-integration-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-data-jpa/target/spring-zero-starter-data-jpa-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter/target/spring-zero-starter-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-actuator/target/spring-zero-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-cli/target/spring-zero-cli-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter/target/spring-zero-starter-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-web/target/spring-zero-starter-web-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-actuator/target/spring-zero-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-launcher/target/spring-zero-launcher-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-security/target/spring-zero-starter-security-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-maven-plugin/target/spring-zero-maven-plugin-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-actuator/target/spring-zero-starter-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-data-jpa/target/spring-zero-starter-data-jpa-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-core/target/spring-zero-core-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-batch/target/spring-zero-starter-batch-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-integration/target/spring-zero-starter-integration-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-autoconfigure/target/spring-zero-autoconfigure-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-cli/target/spring-zero-cli-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter/target/spring-zero-starter-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-web/target/spring-zero-starter-web-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-actuator/target/spring-zero-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-launcher/target/spring-zero-launcher-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-security/target/spring-zero-starter-security-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-maven-plugin/target/spring-zero-maven-plugin-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-actuator/target/spring-zero-starter-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-data-jpa/target/spring-zero-starter-data-jpa-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-core/target/spring-zero-core-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-batch/target/spring-zero-starter-batch-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-integration/target/spring-zero-starter-integration-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-autoconfigure/target/spring-zero-autoconfigure-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-cli/target/spring-zero-cli-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter/target/spring-zero-starter-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-web/target/spring-zero-starter-web-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-actuator/target/spring-zero-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-launcher/target/spring-zero-launcher-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-security/target/spring-zero-starter-security-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-maven-plugin/target/spring-zero-maven-plugin-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-actuator/target/spring-zero-starter-actuator-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-data-jpa/target/spring-zero-starter-data-jpa-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-core/target/spring-zero-core-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-batch/target/spring-zero-starter-batch-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-starters/spring-zero-starter-integration/target/spring-zero-starter-integration-0.5.0.BUILD-SNAPSHOT.jar" />
    <classpathentry kind="lib" path="spring-zero-autoconfigure/target/spring-zero-autoconfigure-0.5.0.BUILD-SNAPSHOT.jar" />
  </classpath>
  <pom-root-files>
    <pom path="/Users/pwebb/projects/spring/spring-bootstrap/code/pom.xml" />
  </pom-root-files>
  <excludes>
    <exclude expression="org.apache.ivy.*" active="true" />
    <exclude expression="antlr.*" active="true" />
    <exclude expression="groovy.*" active="true" />
    <exclude expression="org.codehaus.*" active="true" />
    <exclude expression="joptsimple.*" active="true" />
    <exclude expression="jopts.*" active="true" />
    <exclude expression="org.objectweb.*" active="true" />
  </excludes>
  <transformations>
    <transformation in="*" out="{jar}.*" />
  </transformations>
  <restructuring>
    <set version="2" name="List 1" hiview="Codemap" active="true" todo="false" />
  </restructuring>
  <sourcepaths>
    <pathentry type="file" path="/Users/pwebb/projects/spring/spring-bootstrap/code/spring-zero-cli/src/main/java" />
    <pathentry type="file" path="/Users/pwebb/projects/spring/spring-bootstrap/code/spring-zero-actuator/src/main/java" />
    <pathentry type="file" path="/Users/pwebb/projects/spring/spring-bootstrap/code/spring-zero-launcher/src/main/java" />
    <pathentry type="file" path="/Users/pwebb/projects/spring/spring-bootstrap/code/spring-zero-maven-plugin/src/main/java" />
    <pathentry type="file" path="/Users/pwebb/projects/spring/spring-bootstrap/code/spring-zero-core/src/main/java" />
    <pathentry type="file" path="/Users/pwebb/projects/spring/spring-bootstrap/code/spring-zero-autoconfigure/src/main/java" />
  </sourcepaths>
  <grid-set sep="." version="3.5.4164">
    <grid name="Diagram 1" enforce="true" strict="false">
      <row>
        <cell name="spring-zero-actuator" pattern="spring-zero.spring-zero-actuator.*" vexpanded="true" visibility="public" drill="false">
          <grid>
            <row>
              <cell name="spring-zero-actuator" pattern="spring-zero.spring-zero-actuator.spring-zero-actuator.*" vexpanded="false" visibility="public" drill="false" />
            </row>
          </grid>
        </cell>
      </row>
      <row>
        <cell name="spring-zero-autoconfigure" pattern="spring-zero.spring-zero-autoconfigure.*" vexpanded="true" visibility="public" drill="false">
          <grid>
            <row>
              <cell name="spring-zero-autoconfigure" pattern="spring-zero.spring-zero-autoconfigure.spring-zero-autoconfigure.*" vexpanded="true" visibility="public" drill="false">
                <grid>
                  <row>
                    <cell name="org.springframework.zero" pattern="spring-zero.spring-zero-autoconfigure.spring-zero-autoconfigure.org.springframework.zero.*" vexpanded="true" visibility="public" drill="false">
                      <grid>
                        <row>
                          <cell name="main" pattern="spring-zero.spring-zero-autoconfigure.spring-zero-autoconfigure.org.springframework.zero.main.?" vexpanded="false" visibility="public" drill="false" />
                        </row>
                        <row>
                          <cell name="autoconfigure" pattern="spring-zero.spring-zero-autoconfigure.spring-zero-autoconfigure.org.springframework.zero.autoconfigure.*" vexpanded="false" visibility="public" drill="false" />
                        </row>
                      </grid>
                    </cell>
                  </row>
                </grid>
              </cell>
            </row>
          </grid>
        </cell>
        <cell name="spring-zero-cli" pattern="spring-zero.spring-zero-cli.*" vexpanded="true" visibility="public" drill="false">
          <grid>
            <row>
              <cell name="spring-zero-cli" pattern="spring-zero.spring-zero-cli.spring-zero-cli.*" vexpanded="false" visibility="public" drill="false">
                <grid>
                  <row>
                    <cell name="org" pattern="spring-zero.spring-zero-cli.spring-zero-cli.org.*" vexpanded="false" visibility="public" drill="false">
                      <grid>
                        <row>
                          <cell name="apache.commons.cli" pattern="spring-zero.spring-zero-cli.spring-zero-cli.org.apache.commons.cli.?" vexpanded="false" visibility="public" drill="false" />
                          <cell name="springframework.zero.cli" pattern="spring-zero.spring-zero-cli.spring-zero-cli.org.springframework.zero.cli.*" vexpanded="false" visibility="public" drill="false" />
                        </row>
                      </grid>
                    </cell>
                  </row>
                </grid>
              </cell>
            </row>
          </grid>
        </cell>
      </row>
      <row>
        <cell name="Other Spring Projects" pattern="" vexpanded="false" visibility="public" drill="false" />
      </row>
      <row>
        <cell name="spring-zero-core" pattern="spring-zero.spring-zero-core.*" vexpanded="true" visibility="public" drill="false">
          <grid>
            <row>
              <cell name="spring-zero-core" pattern="spring-zero.spring-zero-core.spring-zero-core.*" vexpanded="true" visibility="public" drill="false">
                <grid>
                  <row>
                    <cell name="org.springframework.zero" pattern="spring-zero.spring-zero-core.spring-zero-core.org.springframework.zero.*" vexpanded="true" visibility="public" drill="false">
                      <grid>
                        <row>
                          <cell name="web" pattern="spring-zero.spring-zero-core.spring-zero-core.org.springframework.zero.web.?" vexpanded="false" visibility="public" drill="false" />
                        </row>
                        <row>
                          <cell name="context" pattern="spring-zero.spring-zero-core.spring-zero-core.org.springframework.zero.context.*" vexpanded="false" visibility="public" drill="false" />
                        </row>
                        <row>
                          <cell name="zero" pattern="spring-zero.spring-zero-core.spring-zero-core.org.springframework.zero.?" vexpanded="false" visibility="public" drill="false" />
                          <cell name="bind" pattern="spring-zero.spring-zero-core.spring-zero-core.org.springframework.zero.bind.?" vexpanded="false" visibility="public" drill="false" />
                          <cell name="config" pattern="spring-zero.spring-zero-core.spring-zero-core.org.springframework.zero.config.?" vexpanded="false" visibility="public" drill="false" />
                          <cell name="logging" pattern="spring-zero.spring-zero-core.spring-zero-core.org.springframework.zero.logging.?" vexpanded="false" visibility="public" drill="false" />
                        </row>
                      </grid>
                    </cell>
                  </row>
                </grid>
              </cell>
            </row>
          </grid>
        </cell>
      </row>
      <row>
        <cell name="Spring Framework" pattern="org.springframework.core.*" vexpanded="true" visibility="public" drill="false" />
      </row>
      <row>
        <cell name="spring-zero-maven-plugin" pattern="spring-zero.spring-zero-maven-plugin.*" vexpanded="false" visibility="public" drill="false">
          <grid>
            <row>
              <cell name="spring-zero-maven-plugin" pattern="spring-zero.spring-zero-maven-plugin.spring-zero-maven-plugin.*" vexpanded="true" visibility="public" drill="false">
                <grid>
                  <row>
                    <cell name="org.springframework.zero.maven" pattern="spring-zero.spring-zero-maven-plugin.spring-zero-maven-plugin.org.springframework.zero.maven.?" vexpanded="true" visibility="public" drill="false">
                      <grid>
                        <row>
                          <cell name="ExecutableJarMojo" pattern="spring-zero.spring-zero-maven-plugin.spring-zero-maven-plugin.org.springframework.zero.maven.ExecutableJarMojo" vexpanded="false" visibility="public" drill="false" />
                          <cell name="ExecutableWarMojo" pattern="spring-zero.spring-zero-maven-plugin.spring-zero-maven-plugin.org.springframework.zero.maven.ExecutableWarMojo" vexpanded="false" visibility="public" drill="false" />
                        </row>
                        <row>
                          <cell name="AbstractExecutableArchiveMojo" pattern="spring-zero.spring-zero-maven-plugin.spring-zero-maven-plugin.org.springframework.zero.maven.AbstractExecutableArchiveMojo" vexpanded="false" visibility="public" drill="false" />
                        </row>
                        <row>
                          <cell name="MainClassFinder" pattern="spring-zero.spring-zero-maven-plugin.spring-zero-maven-plugin.org.springframework.zero.maven.MainClassFinder" vexpanded="false" visibility="public" drill="false" />
                          <cell name="PropertiesMergingResourceTransformer" pattern="spring-zero.spring-zero-maven-plugin.spring-zero-maven-plugin.org.springframework.zero.maven.PropertiesMergingResourceTransformer" vexpanded="false" visibility="public" drill="false" />
                        </row>
                      </grid>
                    </cell>
                  </row>
                </grid>
              </cell>
            </row>
          </grid>
        </cell>
      </row>
      <row>
        <cell name="spring-zero-launcher" pattern="spring-zero.spring-zero-launcher.*" vexpanded="false" visibility="public" drill="false">
          <grid>
            <row>
              <cell name="spring-zero-launcher" pattern="spring-zero.spring-zero-launcher.spring-zero-launcher.*" vexpanded="false" visibility="public" drill="false" />
            </row>
          </grid>
        </cell>
      </row>
      <description>Top-level breakout (depth=2)</description>
    </grid>
  </grid-set>
</local-project>

