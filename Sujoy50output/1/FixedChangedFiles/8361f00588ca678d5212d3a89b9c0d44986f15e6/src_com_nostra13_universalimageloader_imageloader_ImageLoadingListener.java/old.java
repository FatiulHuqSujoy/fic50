package com.nostra13.universalimageloader.imageloader;

/**
 * Listener for image loading process
 * 
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public interface ImageLoadingListener {

	/** Is called when image
	void onLoadingStarted();

	void onLoadingComplete();
}
