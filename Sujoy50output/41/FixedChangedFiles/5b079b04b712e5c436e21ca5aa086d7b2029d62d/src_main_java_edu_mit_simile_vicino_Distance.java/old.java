package edu.mit.simile.vicino;

public interface Distance {

    public float d(String x, String y);

}
