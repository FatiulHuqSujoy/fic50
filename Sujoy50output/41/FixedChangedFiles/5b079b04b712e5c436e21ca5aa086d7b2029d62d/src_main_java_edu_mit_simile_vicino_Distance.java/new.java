package edu.mit.simile.vicino;

public interface Distance {

    public double d(String x, String y);

}
