package com.chad.baserecyclerviewadapterhelper.entity;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class Video {
    public String img;
    public String name;

    public Video(String img, String name) {
        this.img = img;
        this.name = name;
    }
}
