package io.realm.instrumentation;

/**
 * Created by Nabil on 09/09/15.
 */
public interface Lifecycle {
    void onStart ();
    void onStop ();
}
