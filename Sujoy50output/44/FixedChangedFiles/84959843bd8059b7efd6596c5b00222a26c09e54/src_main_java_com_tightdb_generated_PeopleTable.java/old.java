/* This file was automatically generated by TightDB. */

package com.tightdb.generated;


import com.tightdb.*;
import com.tightdb.lib.*;

/**
 * This class represents a TightDB table and was automatically generated.
 */
public class PeopleTable extends AbstractTable<People, PeopleView, PeopleQuery> {

	public static final EntityTypes<PeopleTable, PeopleView, People, PeopleQuery> TYPES = new EntityTypes<PeopleTable, PeopleView, People, PeopleQuery>(PeopleTable.class, PeopleView.class, People.class, PeopleQuery.class); 

	public final StringRowsetColumn<People, PeopleView, PeopleQuery> name = new StringRowsetColumn<People, PeopleView, PeopleQuery>(TYPES, table, 0, "name");
	public final LongRowsetColumn<People, PeopleView, PeopleQuery> age = new LongRowsetColumn<People, PeopleView, PeopleQuery>(TYPES, table, 1, "age");
	public final BooleanRowsetColumn<People, PeopleView, PeopleQuery> hired = new BooleanRowsetColumn<People, PeopleView, PeopleQuery>(TYPES, table, 2, "hired");

	public PeopleTable() {
		super(TYPES);
	}
	
	public PeopleTable(Group group) {
		super(TYPES, group);
	}

	@Override
	protected void specifyStructure(TableSpec spec) {
        registerStringColumn(spec, "name");
        registerLongColumn(spec, "age");
        registerBooleanColumn(spec, "hired");
    }

    #render_method($params)

    #render_method($params)


}
