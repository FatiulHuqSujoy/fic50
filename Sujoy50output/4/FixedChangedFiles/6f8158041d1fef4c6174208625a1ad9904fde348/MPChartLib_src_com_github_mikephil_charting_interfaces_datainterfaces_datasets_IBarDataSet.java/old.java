package com.github.mikephil.charting.interfaces.datainterfaces.datasets;

import com.github.mikephil.charting.data.BarEntry;

/**
 * Created by philipp on 21/10/15.
 */
public interface IBarDataSet extends IBarLineScatterCandleBubbleDataSet<BarEntry> {
}
