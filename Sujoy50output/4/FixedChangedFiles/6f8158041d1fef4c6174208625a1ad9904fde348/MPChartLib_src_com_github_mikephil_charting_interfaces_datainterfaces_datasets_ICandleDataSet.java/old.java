package com.github.mikephil.charting.interfaces.datainterfaces.datasets;

import com.github.mikephil.charting.data.CandleEntry;

/**
 * Created by philipp on 21/10/15.
 */
public interface ICandleDataSet extends IBarLineScatterCandleBubbleDataSet<CandleEntry> {
}
