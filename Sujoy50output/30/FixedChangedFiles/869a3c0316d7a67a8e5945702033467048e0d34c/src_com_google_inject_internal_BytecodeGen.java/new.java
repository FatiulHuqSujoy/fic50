/**
 * Copyright (C) 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.inject.internal;

import static com.google.inject.internal.ReferenceType.WEAK;
import static java.lang.reflect.Modifier.PROTECTED;
import static java.lang.reflect.Modifier.PUBLIC;
import java.security.AccessController;
import java.security.PrivilegedAction;
import net.sf.cglib.core.DefaultNamingPolicy;
import net.sf.cglib.core.NamingPolicy;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.reflect.FastClass;

/**
 * Utility methods for runtime code generation and class loading. We use this stuff for faster
 * reflection ({@link FastClass}), method interceptors ({@link Enhancer}) and to proxy circular
 * dependencies.
 *
 * <p>When loading classes, we need to be careful of:
 * <ul>
 *   <li><strong>Memory leaks.</strong> Generated classes need to be garbage collected in long-lived
 *       applications. Once an injector and any instances it created can be garbage collected, the
 *       corresponding generated classes should be collectable.
 *   <li><strong>Visibility.</strong> Containers like <code>OSGi</code> use class loader boundaries
 *       to enforce modularity at runtime.
 * </ul>
 *
 * <p>For each generated class, there's multiple class loaders involved:
 * <ul>
 *    <li><strong>The related class's class loader.</strong> Every generated class services exactly
 *        one user-supplied class. This class loader must be used to access members with private and
 *        package visibility.
 *    <li><strong>Guice's class loader.</strong>
 *    <li><strong>Our bridge class loader.</strong> This is a child of the user's class loader. It
 *        selectively delegates to either the user's class loader (for user classes) or the Guice
 *        class loader (for internal classes that are used by the generated classes). This class
 *        loader that owns the classes generated by Guice.
 * </ul>
 *
 * @author mcculls@gmail.com (Stuart McCulloch)
 * @author jessewilson@google.com (Jesse Wilson)
 */
public final class BytecodeGen {

  static final ClassLoader GUICE_CLASS_LOADER = BytecodeGen.class.getClassLoader();

  /** ie. "com.google.inject.internal" */
  private static final String GUICE_INTERNAL_PACKAGE
      = BytecodeGen.class.getName().replaceFirst("\\.internal\\..*$", ".internal");

  /** either "net.sf.cglib", or "com.google.inject.internal.cglib" */
  private static final String CGLIB_PACKAGE
      = Enhancer.class.getName().replaceFirst("\\.cglib\\..*$", ".cglib");

  static final NamingPolicy NAMING_POLICY = new DefaultNamingPolicy() {
    @Override protected String getTag() {
      return "ByGuice";
    }
  };

  /** Use "-Dguice.custom.loader=false" to disable custom classloading. */
  static final boolean HOOK_ENABLED
      = "true".equals(System.getProperty("guice.custom.loader", "true"));

  /**
   * Weak cache of bridge class loaders that make the Guice implementation
   * classes visible to various code-generated proxies of client classes.
   */
  private static final ReferenceCache<ClassLoader, ClassLoader> CLASS_LOADER_CACHE
      = new ReferenceCache<ClassLoader, ClassLoader>(WEAK, WEAK) {
        @Override protected ClassLoader create(final ClassLoader typeClassLoader) {
          return AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
            public ClassLoader run() {
              return new BridgeClassLoader(typeClassLoader);
            }
          });
        }
      };

  /**
   * Returns true if {@code type} can be loaded from our custom class loader.
   * Private/implementation classes are not visible to classes loaded by other
   * class loaders, even when they are in the same package. Therefore we cannot
   * intercept classloading requests for such types.
   */
  private static boolean isHookable(Class<?> type) {
    return (type.getModifiers() & (PROTECTED | PUBLIC)) != 0;
  }

  /**
   * For class loaders, {@code null}, is always an alias to the
   * {@link ClassLoader#getSystemClassLoader() system class loader}.
   */
  private static ClassLoader canonicalize(ClassLoader classLoader) {
    return classLoader != null
        ? classLoader
        : ClassLoader.getSystemClassLoader();
  }

  /**
   * Returns the class loader to host generated classes for {@code type}.
   */
  public static ClassLoader getClassLoader(Class<?> type) {
    return getClassLoader(type, type.getClassLoader());
  }

  private static ClassLoader getClassLoader(Class<?> type, ClassLoader delegate) {
    delegate = canonicalize(delegate);

    if (HOOK_ENABLED && isHookable(type)) {
      return CLASS_LOADER_CACHE.get(delegate);
    }

    return delegate;
  }

  public static FastClass newFastClass(Class<?> type) {
    FastClass.Generator generator = new FastClass.Generator();
    generator.setType(type);
    generator.setClassLoader(getClassLoader(type));
    generator.setNamingPolicy(NAMING_POLICY);
    return generator.create();
  }

  public static Enhancer newEnhancer(Class<?> type) {
    Enhancer enhancer = new Enhancer();
    enhancer.setSuperclass(type);
    enhancer.setUseCache(false); // We do enough caching.
    enhancer.setUseFactory(false);
    enhancer.setNamingPolicy(NAMING_POLICY);
    enhancer.setClassLoader(getClassLoader(type));
    return enhancer;
  }

  /**
   * Loader for Guice-generated classes. For referenced classes, this delegates to either either the
   * user's classloader (which is the parent of this classloader) or Guice's class loader.
   */
  private static class BridgeClassLoader extends ClassLoader {

    public BridgeClassLoader(ClassLoader usersClassLoader) {
      super(usersClassLoader);
    }

    @Override protected Class<?> loadClass(String name, boolean resolve)
        throws ClassNotFoundException {

      // delegate internal requests to Guice class space
      if (name.startsWith(GUICE_INTERNAL_PACKAGE) || name.startsWith(CGLIB_PACKAGE)) {
        try {
          Class<?> clazz = GUICE_CLASS_LOADER.loadClass(name);
          if (resolve) {
            resolveClass(clazz);
          }
          return clazz;
        } catch (Exception e) {
          // fall back to classic delegation
        }
      }

      return super.loadClass(name, resolve);
    }
  }
}
